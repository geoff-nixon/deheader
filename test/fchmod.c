/*
 * Items: fchmod(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <sys/stat.h>

main(int arg, char **argv)
{
    (void)fchmod(0, 0);
}
