/*
 * Items: atexit(
 * Standardized-By: SuS
 * Detected-by: gcc-4.4.3 + Linux
 */

#include <stdlib.h>

main(int arg, char **argv)
{
    atexit(NULL);
}
